# MegaForce

Conjunto de ferramentas para auxiliar nas atividades da PMPR.
 
## Como desenvolver?

1. Clone o repositório?
2. Crie um virtualenv com Python 3.6
3. Ative o virtualenv
4. Instale as dependências
5. Configure a instância com .env
6. Execute os testes

```console
git clone git@gitlab.com:operacionalti/pmpr.git pmpr
cd pmpr
python -m venv .pmpr
source .pmpr/bin/activate
pip install -r requirements-dev.txt
cp contrib/env-sample .env
python manage.py test
```
