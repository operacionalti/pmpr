from django.contrib import admin
from megaforce.pm1.models import Staff, Medal


class StaffModelAdmin(admin.ModelAdmin):

    #list_display = ('rank', 'name', 'document_id', 'date_of_inclusion', 'medals_all')
    list_display = ('document_id', 'rank', 'name', 'date_of_inclusion',
                    'gold_medal', 'silver_medal', 'bronze_medal', 'honorable_mention_medal',
                    'blood_medal', 'west_sentinel_medal', 'special_ops_medal',
                    'humanity_medal', 'cavalry_hero_medal', 'triple_border_sentinel_medal',
                    'military_college_medal', 'cel_dulcidio_medal', 'env_merit_medal')
    date_hierarchy = 'date_of_inclusion'
    search_fields = ('name', 'document_id', 'medals__name')
    list_filter = ('medals__name', 'rank')
    filter_vertical = ('medals', )

    def gold_medal(self, obj):
        return 'ouro' in [medal.name for medal in obj.medals.all()]
    gold_medal.boolean = True
    gold_medal.short_description = 'Ouro'

    def silver_medal(self, obj):
        return 'prata' in [medal.name for medal in obj.medals.all()]
    silver_medal.boolean = True
    silver_medal.short_description = 'Prata'

    def bronze_medal(self, obj):
        return 'bronze' in [medal.name for medal in obj.medals.all()]
    bronze_medal.boolean = True
    bronze_medal.short_description = 'Bronze'

    def honorable_mention_medal(self, obj):
        return 'menção honrosa' in [medal.name for medal in obj.medals.all()]
    honorable_mention_medal.boolean = True
    honorable_mention_medal.short_description = 'Honra'

    def blood_medal(self, obj):
        return 'sangue' in [medal.name for medal in obj.medals.all()]
    blood_medal.boolean = True
    blood_medal.short_description = 'Sangue'

    def west_sentinel_medal(self, obj):
        return 'sentinela do oeste' in [medal.name for medal in obj.medals.all()]
    west_sentinel_medal.boolean = True
    west_sentinel_medal.short_description = 'Sentinela do Oeste'

    def special_ops_medal(self, obj):
        return 'mérito operações especiais' in [medal.name for medal in obj.medals.all()]
    special_ops_medal.boolean = True
    special_ops_medal.short_description = 'Operações Especiais'

    def humanity_medal(self, obj):
        return 'humanidade' in [medal.name for medal in obj.medals.all()]
    humanity_medal.boolean = True
    humanity_medal.short_description = 'Humanidade'

    def cavalry_hero_medal(self, obj):
        return 'heróis da cavalaria' in [medal.name for medal in obj.medals.all()]
    cavalry_hero_medal.boolean = True
    cavalry_hero_medal.short_description = 'Herói da Cavalaria'

    def triple_border_sentinel_medal(self, obj):
        return 'sentinela da tríplice fronteira' in [medal.name for medal in obj.medals.all()]
    triple_border_sentinel_medal.boolean = True
    triple_border_sentinel_medal.short_description = 'Sentinela da Tríplice Fronteira'

    def military_college_medal(self, obj):
        return 'cinquentenário do colégio militar' in [medal.name for medal in obj.medals.all()]
    military_college_medal.boolean = True
    military_college_medal.short_description = 'Cinquentenário do Colégio Militar'

    def cel_dulcidio_medal(self, obj):
        return 'coronel dulcidio - prata' in [medal.name for medal in obj.medals.all()]
    cel_dulcidio_medal.boolean = True
    cel_dulcidio_medal.short_description = 'Coronel Dulcidio - Prata'

    def env_merit_medal(self, obj):
        return 'mérito ambiental' in [medal.name for medal in obj.medals.all()]
    env_merit_medal.boolean = True
    env_merit_medal.short_description = 'Mérito Ambiental'


class MedalModelAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)


admin.site.register(Staff, StaffModelAdmin)
admin.site.register(Medal, MedalModelAdmin)
