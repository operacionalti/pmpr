from datetime import datetime
from django.test import TestCase
from megaforce.pm1.models import Staff

class StaffModelTest(TestCase):
    def setUp(self):
        self.obj = Staff(
            rank='SD',
            name='Adrielle Fernandes Anschau',
            document_id='139702182',
            date_of_inclusion='2013-10-11',
        )
        self.obj.save()
    
    def test_create(self):
        self.assertTrue(Staff.objects.exists())

    def test_created_at(self):
        """Staff must have an auto created_at attr."""
        self.assertIsInstance(self.obj.created_at, datetime)

    def test_updated_at(self):
        """Staff must have an auto updated_at attr."""
        self.assertIsInstance(self.obj.updated_at, datetime)

    def test_str(self):
        self.assertEqual('Adrielle Fernandes Anschau', str(self.obj))

    def test_medals_blank(self):
        field = Staff._meta.get_field('medals')
        self.assertTrue(field.blank)

    def test_name_unique(self):
        field = Staff._meta.get_field('name')
        self.assertTrue(field.unique)

    def test_document_id_unique(self):
        field = Staff._meta.get_field('document_id')
        self.assertTrue(field.unique)
