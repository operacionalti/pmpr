from django.test import TestCase
from megaforce.pm1.models import Medal


class MedalsModelTest(TestCase):
    def setUp(self):
        self.obj = Medal(
            name='Sangue'
        )
        self.obj.save()

    def test_create(self):
        self.assertTrue(Medal.objects.exists())

    def test_str(self):
        self.assertEquals('Sangue', str(self.obj))
