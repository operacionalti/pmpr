from django.apps import AppConfig


class Pm1Config(AppConfig):
    name = 'megaforce.pm1'
    verbose_name = 'PM1'
