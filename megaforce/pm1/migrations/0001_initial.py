# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-14 19:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rank', models.CharField(choices=[('SD', 'Soldado'), ('CB', 'Cabo'), ('3SGT', '3º Sargento'), ('2SGT', '2º Sargento'), ('1SGT', '1º Sargento'), ('SUBTEN', 'Subtenente'), ('2TEN', '2º Tenente'), ('1TEN', '1º Tenente'), ('CAP', 'Capitão'), ('TENCEL', 'Tenente-Coronel'), ('CEL', 'Coronel')], max_length=6)),
                ('name', models.CharField(max_length=255)),
                ('document_id', models.CharField(max_length=10)),
                ('date_of_inclusion', models.DateField(blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
