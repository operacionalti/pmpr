from django.db import models


class Staff(models.Model):
    SD = 'SD'
    CB = 'CB'
    SGT3 = '3SGT'
    SGT2 = '2SGT'
    SGT1 = '1SGT'
    SUBTEN = 'SUBTEN'
    ASPOF = 'ASPOF'
    TEN2 = '2TEN'
    TEN1 = '1TEN'
    CAP = 'CAP'
    MAJ = 'MAJ'
    TENCEL = 'TENCEL'
    CEL = 'CEL'

    RANKS = (
        (SD, 'SD'),
        (CB, 'CB'),
        (SGT3, '3º SGT'),
        (SGT2, '2º SGT'),
        (SGT1, '1º SGT'),
        (SUBTEN, 'SUBTEN'),
        (ASPOF, 'Asp.Of.'),
        (TEN2, '2º Ten.'),
        (TEN1, '1º Ten.'),
        (CAP, 'Cap.'),
        (MAJ, 'Maj.'),
        (TENCEL, 'Ten.-Cel.'),
        (CEL, 'Cel.'),
    )

    rank = models.CharField('posto/graduação', max_length=6, choices=RANKS)
    name = models.CharField('nome', max_length=255, unique=True)
    document_id = models.CharField('RG', max_length=10, unique=True)
    date_of_inclusion = models.DateField(
        'data de inclusão', blank=True, null=True)
    medals = models.ManyToManyField(
        'Medal', verbose_name='medalhas', blank=True)
    created_at = models.DateTimeField('criado em', auto_now_add=True)
    updated_at = models.DateTimeField('atualizado em', auto_now=True)

    class Meta:
        verbose_name = 'efetivo'
        verbose_name_plural = 'efetivo'

    def __str__(self):
        return self.name


class Medal(models.Model):
    name = models.CharField('nome', max_length=255)

    class Meta:
        verbose_name = 'medalha'
        verbose_name_plural = 'medalhas'

    def __str__(self):
        return self.name
