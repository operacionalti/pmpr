# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-16 23:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pm4', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarMaintenance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('km', models.PositiveIntegerField(help_text='quilometragem da viatura na data da manutenção', verbose_name='KM')),
                ('start_date', models.DateTimeField(verbose_name='data de entrada')),
                ('end_date', models.DateTimeField(blank=True, null=True, verbose_name='data de saída')),
                ('maintenance_type', models.CharField(choices=[('engine_oil', 'troca de óleo'), ('break_fluid', 'troca de fluido de freio'), ('timing_belt', 'troca de correia dentada')], help_text='tipo da manutenção', max_length=32, verbose_name='tipo')),
                ('notes', models.TextField(blank=True, null=True, verbose_name='observações')),
                ('car', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pm4.Car', verbose_name='viatura')),
            ],
        ),
    ]
