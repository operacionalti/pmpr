from django.db import models


class CarManufacturer(models.Model):
    name = models.CharField(
        max_length=255,
        unique=True,
        help_text="Fabricante, ex: Ford, GM, Renault, ...")

    class Meta:
        verbose_name = 'fabricante de viatura'
        verbose_name_plural = 'fabricantes de viaturas'
        ordering = ["name"]

    def save(self, *args, **kwargs):
        self.name = self.name.upper()
        super(CarManufacturer, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class CarModel(models.Model):
    manufacturer = models.ForeignKey(
        "CarManufacturer",
        verbose_name="fabricante",
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )

    name = models.CharField(
        max_length=255,
        unique=True
    )

    engine_oil_changing = models.PositiveIntegerField(
        verbose_name='óleo',
        help_text='troca de óleo a cada X km',
        blank=True,
        null=True
    )

    timing_belt_changing = models.PositiveIntegerField(
        verbose_name='correia dentada',
        help_text='troca da correia dentada a cada X km',
        blank=True,
        null=True
    )

    brake_fluid_changing = models.PositiveIntegerField(
        verbose_name='fluido de freio',
        help_text='troca do fluido de freio a cada X km',
        blank=True,
        null=True
    )

    created_at = models.DateTimeField(
        verbose_name='criado em',
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        verbose_name='atualizado em',
        auto_now=True
    )

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.title()
        super(CarModel, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'modelo de viatura'
        verbose_name_plural = 'modelos de viaturas'
        ordering = ['name', 'manufacturer']


class Car(models.Model):
    AVAILABLE = 'available'
    MAINTENANCE = 'maintenance'
    OUT_OF_SERVICE = 'out of service'

    STATUS = (
        (AVAILABLE, 'disponível'),
        (MAINTENANCE, 'manutenção'),
        (OUT_OF_SERVICE, 'fora de serviço')
    )

    name = models.CharField(
        verbose_name='prefixo',
        max_length=255,
        unique=True
    )

    model = models.ForeignKey(
        "CarModel",
        verbose_name='modelo',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    km = models.PositiveIntegerField(
        verbose_name='KM',
        help_text='quilometragem da viatura',
        blank=True,
        null=True
    )

    car_status = models.CharField(
        verbose_name='status',
        help_text='situação da viatura',
        max_length=32,
        choices=STATUS
    )

    created_at = models.DateTimeField(
        verbose_name='criado em',
        auto_now_add=True
    )

    updated_at = models.DateTimeField(
        verbose_name='atualizado em',
        auto_now=True
    )
    # foreign key para Notes
    # será um inline nesse modelo
    # no list_display vai ter um calculated field para a nota mais recente

    class Meta:
        verbose_name = 'viatura'
        verbose_name_plural = 'viaturas'
        ordering = ["name", "model"]

    def __str__(self):
        return self.name


class CarMaintenance(models.Model):
    OIL = 'engine_oil'
    BREAK = 'break_fluid'
    BELT = 'timing_belt'

    KINDS = (
        (OIL, 'troca de óleo'),
        (BREAK, 'troca de fluido de freio' ),
        (BELT, 'troca de correia dentada')
    )

    car = models.ForeignKey(
        "Car",
        verbose_name="viatura",
        on_delete=models.CASCADE
    )

    km = models.PositiveIntegerField(
        verbose_name="KM",
        help_text='quilometragem da viatura na data da manutenção'
        )
    
    start_date=models.DateTimeField(
            verbose_name="data de entrada",
    )

    end_date=models.DateTimeField(
            verbose_name="data de saída",
            blank=True,
            null=True
    )

    maintenance_type = models.CharField(
        verbose_name = 'tipo',
        help_text = 'tipo da manutenção',
        max_length=32,
        choices=KINDS
    )

    notes = models.TextField(
        verbose_name='observações',
        null=True,
        blank=True
    )

    def __str__(self):
        return "{}-{}".format(self.car.name, self.maintenance_type)

    class Meta:
        verbose_name = 'manutenção'
        verbose_name_plural = 'manutenções'
