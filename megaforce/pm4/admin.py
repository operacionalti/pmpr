from django.contrib import admin
from megaforce.pm4.models import CarManufacturer, CarModel, Car, CarMaintenance


class CarManufacturerModelAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )


class CarModelModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'manufacturer', 'engine_oil_changing',
                    'timing_belt_changing', 'brake_fluid_changing')
    search_fields = ('name', )
    list_filter = ('manufacturer', )


class CarModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'model', 'km', 'car_status', 'updated_at',
    'need_oil_changing', 'need_break_fluid_changing', 'need_belt_changing')
    search_fields = ('name', )
    list_filter = ('model', 'car_status', 'updated_at')

    def need_oil_changing(self, obj):
        # most recent engine oil maintenance
        maintenance = obj.carmaintenance_set.filter(
            maintenance_type = CarMaintenance.OIL)
        maintenance_km = maintenance.get().km if maintenance else 0

        return obj.km - maintenance_km < obj.model.engine_oil_changing

    need_oil_changing.boolean = True
    need_oil_changing.short_description = 'óleo'

    def need_belt_changing(self, obj):
        # most recent belt maintenance
        maintenance = obj.carmaintenance_set.filter(
            maintenance_type = CarMaintenance.BELT)
        maintenance_km = maintenance.get().km if maintenance else 0

        return obj.km - maintenance_km < obj.model.timing_belt_changing

    need_belt_changing.boolean = True
    need_belt_changing.short_description = 'correia'

    def need_break_fluid_changing(self, obj):
        # most recent break fluid maintenance
        maintenance = obj.carmaintenance_set.filter(
            maintenance_type = CarMaintenance.BREAK)
        maintenance_km = maintenance.get().km if maintenance else 0

        return obj.km - maintenance_km < obj.model.brake_fluid_changing

    need_break_fluid_changing.boolean = True
    need_break_fluid_changing.short_description = 'fluido de freio'

class CarMaintenanceModelAdmin(admin.ModelAdmin):
    list_display = ('maintenance_type', 'car', 'km', 'start_date', 'end_date', 'notes')
    search_fields = ('car', 'maintenance_type', 'notes')
    list_filter = ('car', 'maintenance_type', 'end_date')
    date_hierarchy = 'start_date'


admin.site.register(CarManufacturer, CarManufacturerModelAdmin)
admin.site.register(CarModel, CarModelModelAdmin)
admin.site.register(Car, CarModelAdmin)
admin.site.register(CarMaintenance, CarMaintenanceModelAdmin)
