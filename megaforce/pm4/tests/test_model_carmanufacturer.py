from django.test import TestCase
from megaforce.pm4.models import CarManufacturer


class CarManufacturerModelTest(TestCase):
    """Test Manufacturer Model"""

    def setUp(self):
        """Creates a common Manufacturer object"""
        self.obj = CarManufacturer(
            name='Renault'
        )
        self.obj.save()

    def test_create(self):
        self.assertTrue(CarManufacturer.objects.exists())

    def test_str(self):
        """str(obj) must return obj.name"""
        self.assertEqual('RENAULT', str(self.obj))

    def test_uppered_name_on_save(self):
        """name field must be uppered when saving obj"""
        m = CarManufacturer.objects.create(name='fiat')
        self.assertEqual('FIAT', m.name)

    def test_name_unique(self):
        """name field must be unique"""
        field = CarManufacturer._meta.get_field('name')
        self.assertTrue(field.unique)
