from django.test import TestCase
from megaforce.pm4.models import Car


class CarModelTest(TestCase):
    """Test Car Model"""

    def setUp(self):
        self.obj = Car(
            name='10399'
        )
        self.obj.save()

    def test_create(self):
        self.assertTrue(Car.objects.exists())
