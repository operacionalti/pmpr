from django.test import TestCase
from megaforce.pm4.models import CarModel


class CarModelModelTest(TestCase):
    """Test CarModel Model"""

    def setUp(self):
        """Creates a common Car object"""
        self.obj = CarModel(
            name='Etios'
        )
        self.obj.save()

    def test_create(self):
        self.assertTrue(CarModel.objects.exists())

    def test_str(self):
        """str(obj) must return obj.name"""
        self.assertEqual('Etios', str(self.obj))

    def test_capitalized_name_on_save(self):
        """name field must be capitalized when saving obj"""
        carmodel = CarModel.objects.create(name='duster')
        self.assertEqual('Duster', carmodel.name)

    '''
    def test_has_manufacturer(self):
        """CarModel belongs to a Manufacturer"""
        self.obj.manufacturer.create(
            name='Renault'
        )
        self.assertEqual(1, self.obj.manufacturer.count())
    '''
