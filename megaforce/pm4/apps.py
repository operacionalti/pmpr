from django.apps import AppConfig


class Pm4Config(AppConfig):
    name = 'megaforce.pm4'
    verbose_name = 'PM4'
