import csv
from megaforce.pm1.models import Medal


gold = Medal.objects.get(name='ouro')
silver = Medal.objects.get(name='prata')
bronze = Medal.objects.get(name='bronze')
honorable_mention = Medal.objects.get(name='menção honrosa')
blood = Medal.objects.get(name='sangue')
west_sentinel = Medal.objects.get(name='sentinela do oeste')
special_ops = Medal.objects.get(name='mérito operações especiais')
humanity = Medal.objects.get(name='humanidade')
cavalry_hero = Medal.objects.get(name='heróis da cavalaria')
triple_border_sentinel = Medal.objects.get(name='sentinela da tríplice fronteira')
military_college = Medal.objects.get(name='cinquentenário do colégio militar')
cel_dulcidio = Medal.objects.get(name='coronel dulcidio - prata')
env_merit = Medal.objects.get(name='mérito ambiental')

fieldnames=('rank', 'name', 'document_id', 'date_of_inclusion', 'gold', 'silver', 'bronze', 'honorable_mention', 'blood', 'west_sentinel', 'special_ops', 'humanity', 'cavalry_hero', 'triple_border_sentinel',
'military_college', 'cel_dulcidio', 'env_merit')
reader = csv.DictReader(open('contrib/medalhas.csv', 'r'), fieldnames)

for line in reader:
    cop = Staff.objects.create(
        rank=line['rank'],
        name=line['name'],
        document_id=line['document_id'],
        date_of_inclusion=line['date_of_inclusion'] or None
    )
    if line['gold']:
        cop.medals.add(gold)
    if line['silver']:
        cop.medals.add(silver)
    if line['bronze']:
        cop.medals.add(bronze)
    if line['honorable_mention']:
        cop.medals.add(honorable_mention)
    if line['blood']:
        cop.medals.add(blood)
    if line['west_sentinel']:
        cop.medals.add(west_sentinel)
    if line['special_ops']:
        cop.medals.add(special_ops)
    if line['humanity']:
        cop.medals.add(humanity)
    if line['cavalry_hero']:
        cop.medals.add(cavalry_hero)
    if line['triple_border_sentinel']:
        cop.medals.add(triple_border_sentinel)
    if line['military_college']:
        cop.medals.add(military_college)
    if line['cel_dulcidio']:
        cop.medals.add(cel_dulcidio)
    if line['env_merit']:
        cop.medals.add(env_merit)
